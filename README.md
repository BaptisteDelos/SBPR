# SBPR

This project presents an implementation of A. Hertzmann work <a href="https://www.mrl.nyu.edu/publications/painterly98" target="_blank">*Painterly Rendering with Curved Brush Strokes of Multiple Sizes*</a>.
It actually relies on **CMake** for building and **OpenCV** for image manipulation.

## Building

```shell
$ mkdir build && cd build
$ cmake ..
$ make -j $(nproc)
```
