#ifndef BSPLINE_H
#define BSPLINE_H

#include <vector>

#include <opencv2/core.hpp>

using namespace cv;


class BSpline
{
public:
    BSpline() {};
    BSpline(int order, std::vector<Vec2i> &controlPoints);

    std::vector<Vec2i> getCurve(int resolution) const;

    std::vector<Vec2i> getControlPoints() const;

private:
    int                 _order;
    std::vector<Vec2i>  _controlPoints;
    std::vector<int>    _nodalVector;

    Vec2i getPosition(float u) const;
    int getShift(float u) const;
};

#endif // BSPLINE_H
