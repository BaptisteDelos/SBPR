#include <iostream>
#include <algorithm>
#include <random>
#include <unistd.h>
#include <iomanip>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "stroke.h"


const char *app_name {"SBPR"};

/*  User inputs  *
 *---------------*/

float smallest_radius = 3.0f;
int brush_size_count = 6;
float brush_size_ratio = 1.5f;
float f_sigma = 3.0f;
float f_grid = 0.5f;
float error_threshold = 50.0f;


/*  Utils  *
 *---------*/

void getChannel(const Mat &src, int convert_code, int channel_index, Mat &channel) {
    Mat cvt_mat;
    cvtColor(src, cvt_mat, convert_code);
    Mat channels[cvt_mat.channels()];
    split(cvt_mat, channels);
    channel = channels[channel_index];
}

double getNumericMaxLimit(int depth) {
    double maxima[7] {
        double(std::numeric_limits<unsigned char>().max()),  // CV_8U
        double(std::numeric_limits<char>().max()),           // CV_8S
        double(std::numeric_limits<unsigned short>().max()), // CV_16U
        double(std::numeric_limits<short>().max()),          // CV_16S
        double(std::numeric_limits<long>().max()),           // CV_32S
        double(std::numeric_limits<float>().max()),          // CV_32F
        double(std::numeric_limits<double>().max()),         // CV_64F
    };

    return maxima[depth];
}


/*  Painting procedures  *
 *-----------------------*/

int display(const Mat &img, int delay_ms=0, int convertType=-1, const char *windowName=app_name) {
    Mat cvt_img(img);

    if (convertType != -1) {
        img.convertTo(cvt_img, convertType);
    }

    imshow(windowName, cvt_img);

    return waitKey(delay_ms);
}

/**
 * @brief paintLayer Computes and applies a series of strokes on the current layer of the canvas, given the source and reference images, and the gradient field of the latter
 *
 * This function implements the paintLayer procedure described in the original paper.
 *
 * @param canvas 32-bit canvas on which strokes are drawn
 * @param sourceImage 32-bit floating original image
 * @param referenceImage Reference image approximated by application of strokes
 * @param radius Current radius of the strokes being drawn on the layer
 * @param gradientMag Gradient magnitude of the original image
 * @param sx, sy Horizontal and vertical gradients of the original image
 */
void paintLayer(Mat &canvas, const Mat &sourceImage, const Mat &referenceImage, float radius, const Mat &gradientMag, const Mat &sx, const Mat &sy) {
    std::vector<Stroke *> strokes;

    // Use L2 distance as color difference function
    Mat diffImage = canvas - referenceImage;
    diffImage = diffImage.mul(diffImage);
    transform(diffImage, diffImage, Matx13f(1, 1, 1));
    sqrt(diffImage, diffImage);

    int step = static_cast<int>(f_grid * radius);
    int half_step = static_cast<int>(float(step) * 0.5f);
    float inv_square_step = 1.0f / float(step * step);

    for (int i = half_step; i < canvas.rows - half_step; i += step)
    {
        for (int j = half_step; j < canvas.cols - half_step; j += step)
        {
            Mat diffNeighborhood;
            diffImage(Rect(j-half_step, i-half_step, step, step)).copyTo(diffNeighborhood);
            diffNeighborhood *= 255.f;
            float areaError = float(sum(diffNeighborhood)[0]) * inv_square_step;

            if (canvas.at<Vec3f>(i,j)[0] < 0 || areaError > error_threshold) {
                Point max_loc;
                minMaxLoc(diffNeighborhood, nullptr, nullptr, nullptr, &max_loc);
                Point2i stroke_pos(i - half_step + max_loc.y, j - half_step + max_loc.x);
                strokes.emplace_back(new SplineStroke(radius, stroke_pos.x, stroke_pos.y, canvas, sourceImage, referenceImage, gradientMag, sx, sy));
            }
        }
    }

    // Randomly shuffle the order in which strokes will be drawn
    int n = strokes.size();
    int stroke_indices[n];
    unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();

    for (int i = 0; i < n; ++i) {
        stroke_indices[i] = i;
    }
    std::shuffle(stroke_indices, stroke_indices + n, std::default_random_engine(seed));

    // Draw
    for (int stroke_index : stroke_indices)
    {
        strokes[stroke_index]->draw(canvas);
    }
}

/**
 * @return the result of Hertzmann's multi-resolution painting algorithm
 *
 * This function implements the paint procedure of the original paper.
 *
 * @param radii List of stroke radii, in decreasing order, used for each layer
 */
Mat paint(const Mat &sourceImage, std::vector<float> &radii) {
    Mat canvas(sourceImage.rows, sourceImage.cols, CV_32FC3, Scalar(-1.0f, -1.0f, -1.0f));
    Mat reference;

    for (int i = 0; i < radii.size(); ++i)
    {
        // Compute reference image
        float sigma = f_sigma * radii[i];
        GaussianBlur(sourceImage, reference, Size(5,5), sigma, sigma); // Arbitrary kernel size for now

        // Compute luminance
        Mat gray;
        getChannel(reference, COLOR_RGB2GRAY, 0, gray);

        // Compute Sobel gradients and magnitude
        Mat sx, sy, mag, orient;
        Sobel(gray, sx, CV_32F, 1, 0);
        Sobel(gray, sy, CV_32F, 0, 1);
        magnitude(sx, sy, mag);

        // Paint layer
        paintLayer(canvas, sourceImage, reference, radii[i], mag, sx, sy);
    }

    reference.release();

    return canvas;
}

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " <Image_Path>" << std::endl;
        return -1;
    }

    Mat image;
    image = imread(argv[1], 1);

    if (!image.data) {
        std::cerr << "No image data" << std::endl;
        return -1;
    }

    namedWindow("SBPR", WINDOW_AUTOSIZE);

    // Convert image elements to 32-bit floating-point precision in range [0, 1],
    // thus allow further treatments to be generic
    double max_limit = getNumericMaxLimit(image.depth());
    image.convertTo(image, CV_32FC3);
    image /= max_limit;

    // Compute radii array
    std::vector<float> radii(brush_size_count);
    radii[brush_size_count - 1] = smallest_radius;

    for (int i = brush_size_count - 2; i >= 0; --i) {
        radii[i] = radii[i + 1] * brush_size_ratio;
    }

    Mat result;

    do {
        result = paint(image, radii);
    } while(display(result) != 27);

    image.release();
    result.release();

    return 0;
}
