#include "stroke.h"

#include <algorithm>
#include <iostream>

#include <opencv2/imgproc.hpp>


/*  Stroke interface                                        *
 *----------------------------------------------------------*/

Stroke::Stroke(Vec3f color, float radius) :
    _color(color), _radius(radius)
{

}


/*  Round stroke                                            *
 *----------------------------------------------------------*/

RoundStroke::RoundStroke(float radius, int x, int y, const Mat &referenceImage) :
    Stroke(referenceImage.at<Vec3f>(x, y), radius), _center(y, x)
{

}

void RoundStroke::draw(Mat &canvas) const
{
    cv::circle(canvas, _center, _radius, _color, FILLED, LINE_AA);
}


/*  B-Spline stroke                                         *
 *----------------------------------------------------------*/

SplineStroke::SplineStroke(float radius, int x0, int y0, const Mat &canvas, const Mat &sourceImage, const Mat &referenceImage, const Mat &gradientMag, const Mat &sx, const Mat &sy) :
    Stroke(referenceImage.at<Vec3f>(x0, y0), radius)
{
    std::vector<Vec2i> controlPoints;
    controlPoints.emplace_back(Vec2i(y0, x0));
    Vec2i point(x0, y0);
    Vec2f last_dir(0, 0);

    for (int i = 1; i < max_stroke_length; ++i)
    {
        // Return stroke if last control point lies outside the image
        if (point[0] < 0 || point[0] >= canvas.rows || point[1] < 0 || point[1] >= canvas.cols) {
            break;
        }

        float distToCanvasColor = cv::norm(sourceImage.at<Vec3f>(point) - canvas.at<Vec3f>(point));
        float distToStrokeColor = cv::norm(sourceImage.at<Vec3f>(point) - _color);

        // Return stroke if its color differs from the reference color more than this differs from canvas color
        if (i > min_stroke_length && distToCanvasColor < distToStrokeColor) {
            break;
        }

        // Detect vanishing gradient
        if (gradientMag.at<float>(point) < std::numeric_limits<float>::epsilon()) {
            break;
        }

        Vec2f gradient_dir;
        normalize(Vec2f(sy.at<float>(point), sx.at<float>(point)), gradient_dir);

        // Compute normal to gradient direction
        Vec2f gradient_normal(-gradient_dir[1], gradient_dir[0]);

        // Reverse direction if needed (minimize stroke curvature)
        if (last_dir.ddot(gradient_normal) < 0.0) {
            gradient_normal *= -1.0f;
        }

        // Filter stroke direction
        gradient_normal = f_curvature * gradient_normal + (1.0f - f_curvature) * last_dir;
        cv::normalize(gradient_normal, gradient_normal);
        point = Vec2i(point[0] + static_cast<int>(radius * gradient_normal[0]), point[1] + static_cast<int>(radius * gradient_normal[1]));
        last_dir = gradient_normal;
        controlPoints.emplace_back(Vec2i(point[1], point[0]));
    }

    int npoints = controlPoints.size();
    _curve = BSpline(std::min(npoints, 3), controlPoints);
}

void SplineStroke::draw(Mat &canvas) const
{
    auto curve = _curve.getCurve(spline_resolution);
    cv::polylines(canvas, curve, false, _color, _radius, LINE_AA);
}
