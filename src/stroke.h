#ifndef STROKE_H
#define STROKE_H

#include <vector>

#include <opencv2/core.hpp>

#include "bspline.h"

using namespace cv;

// User inputs
static float f_curvature = 0.5f;
static int min_stroke_length = 2;
static int max_stroke_length = 16;
static int spline_resolution = 16;


/*  Stroke interface                                        *
 *----------------------------------------------------------*/

class Stroke
{
public:
    Stroke(Vec3f color, float radius);

    virtual void draw(Mat &canvas) const = 0;

protected:
    Vec3f _color;
    float _radius;
};


/*  Round stroke                                            *
 *----------------------------------------------------------*/

class RoundStroke : public Stroke
{
public:
    RoundStroke(float radius, int x, int y, const Mat &referenceImage);

    virtual void draw(Mat &canvas) const override;

private:
    Vec2i _center;
};


/*  B-Spline stroke                                         *
 *----------------------------------------------------------*/

class SplineStroke : public Stroke
{
public:
    SplineStroke(float radius, int x0, int y0, const Mat &canvas, const Mat &sourceImage, const Mat &referenceImage, const Mat &gradientMag, const Mat &sx, const Mat &sy);

    virtual void draw(Mat &canvas) const override;

private:
    BSpline _curve;
};


#endif // STROKE_H
