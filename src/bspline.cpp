#include "bspline.h"

#include <iostream>

BSpline::BSpline(int order, std::vector<Vec2i> &controlPoints) :
    _order(order), _controlPoints(controlPoints)
{
    int n = _controlPoints.size() - 1;

    // Use an open uniform nodal vector
    for (int i = 0; i < _order; ++i)
        _nodalVector.emplace_back(0);
    for (int i = 1; i <= n - _order + 1; ++i)
        _nodalVector.emplace_back(i);
    for (int i = 0; i < _order; ++i)
        _nodalVector.emplace_back(n - _order + 2);
}

std::vector<Vec2i> BSpline::getCurve(int resolution) const
{
    float u_min = _nodalVector[_order - 1];
    float u_max = _nodalVector[_controlPoints.size()];
    float u_step = (u_max - u_min) / static_cast<float>(resolution);
    std::vector<Vec2i> curve;

    for (float u = u_min; u <= u_max; u += u_step)
    {
        curve.emplace_back(getPosition(u));
    }

    return curve;
}

std::vector<Vec2i> BSpline::getControlPoints() const
{
    return _controlPoints;
}

/**
 * @brief Evaluates the B-Spline curve P(u) for a given u
 *
 * This function computes the 2D position on the curve given its u parameter value, performing a version of the Cox de Boor blossom recursion
 *
 * @param u parameter of the curve
 * @return the corresponding 2D point on the curve
 */
Vec2i BSpline::getPosition(float u) const
{
    int shift = getShift(u);
    Vec2f blossom_func[_order];

    std::copy(_controlPoints.begin() + shift, _controlPoints.begin() + shift + _order, blossom_func);

    for (int iter = 0; iter < _order; ++iter)
    {
        for (int j = 0; j < _order - iter - 1; ++j)
        {
            float u_max = _nodalVector[_order + shift + j];
            float u_min = _nodalVector[shift + 1 + j + iter];
            float factor = 1.0f / static_cast<float>(u_max - u_min);
            blossom_func[j] *= (u_max - u) * factor;
            blossom_func[j] += (u - u_min) * blossom_func[j + 1] * factor;
        }
    }

    return blossom_func[0];
}

int BSpline::getShift(float u) const
{
    int shift = 0;

    for (int i = _order; i < _nodalVector.size() && u > _nodalVector[i]; ++i, ++shift) {}

    return shift;
}
